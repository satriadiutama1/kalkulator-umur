import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor:Colors.blue[50],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          title: Text('Profile Diriku'),
          centerTitle: true,
          leading: Icon(
            Icons.home,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              color: Colors.white,
              onPressed: () {},
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Picture(),
              TextNama(),
              TextNIM(),
              FirstRow(),
              SecondRow(),
            ],
          ),
        ),
      ),
    );
  }
}

class Picture extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      margin: const EdgeInsets.only(top: 30.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.100),
        image: DecorationImage(
          image: AssetImage('assets/satria.jpg'),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class TextNama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "Gede Satriadi Utama",
        style: TextStyle(
          fontSize: 24,
          color: Colors.black,
        ),
      ),
      margin: const EdgeInsets.only(top: 20.0),
    );
  }
}

class TextNIM extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "1915051075",
        style: TextStyle(
          fontSize: 24,
          color: Colors.black,
        ),
      ),
      margin: const EdgeInsets.only(top: 20.0),
    );
  }
}

class FirstRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: new LinearGradient(colors:
            [Colors.red,Colors.cyan],
            begin: Alignment.centerRight,
            end: Alignment(-1.0,-1.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                spreadRadius: 0.5,
              ),
            ],
          ),
          width: 170,
          margin: const EdgeInsets.only(top: 50.0),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18.0),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.call,
                    size: 50,
                    color: Colors.green,
                  ),
                  Text(
                    'Contact',
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  )
                ],
              ),
            ),
          ),
        ),

        Container(
          decoration: BoxDecoration(
            gradient: new LinearGradient(colors:
            [Colors.red,Colors.cyan],
                begin: Alignment.centerRight,
                end: Alignment(-1.0,-1.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                spreadRadius: 0.5,
              ),
            ],
          ),
          width: 170,
          margin: const EdgeInsets.only(top: 50.0),
          child: Card(
              child: Padding(
                padding: EdgeInsets.all(18.0),
                child: Column(
                  children: <Widget>[
                    Icon(
                      Icons.videogame_asset,
                      size: 50,
                      color: Colors.red,
                    ),
                    Text(
                      'Games',
                      style: TextStyle(color: Colors.black, fontSize: 18),
                    )
                  ],
                ),
              )
          ),
        ),
      ],
    );
  }
}

class SecondRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: new LinearGradient(colors:
            [Colors.red,Colors.cyan],
                begin: Alignment.centerRight,
                end: Alignment(-1.0,-1.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                spreadRadius: 0.5,
              ),
            ],
          ),
          width: 170,
          margin: const EdgeInsets.only(top: 20.0),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18.0),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.school,
                    size: 50,
                    color: Colors.blue,
                  ),
                  Text(
                    'PTI',
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  )
                ],
              ),
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            gradient: new LinearGradient(colors:
            [Colors.red,Colors.cyan],
                begin: Alignment.centerRight,
                end: Alignment(-1.0,-1.0)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                spreadRadius: 0.5,
              ),
            ],
          ),
          width: 170,
          margin: const EdgeInsets.only(top: 20.0),
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(18.0),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.movie,
                    size: 50,
                    color: Colors.brown,
                  ),
                  Text(
                    'Editing',
                    style: TextStyle(color: Colors.black, fontSize: 18),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}